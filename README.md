## INTRODUCTION

In many countries and regions, the mailbox usage rate is not high, and the
mailbox is not necessary, but in Drupal's core user entity, the mailbox field is
required, User Mail Control module can delete or cancel the required field in
the registration form, and set the field in the user edit page as non-mandatory.

## Implementation functions:

1. You can set a default custom domain name, and automatically create a default
   email address for new users when registering, for example:
   userName@example.com;
2. The email field in the user editing interface can be set as a non-required
   field;
3. The email field of the user registration form can be set as optional or
   deleted;
4. Creating a new user does not verify whether the user has filled in the
   mailbox;

在许多国家和地区，邮箱的使用率并不高，邮箱也不是必须的，但Drupal核心的用户实体中，邮箱字段是必填的，User
Mail Control模块可以在注册表单中把邮箱字段删除或是取消必填，在用户编辑页面中设置邮箱字段非必填。
实现功能：

1. 您可以设置一个默认的自定义域名，注册时自动为新用户创建默认邮箱，例如：userName@example.com；
2. 用户编辑界面中的邮件字段可以设置为非必填字段；
3. 用户注册表单的邮件字段可以设置为非必填项，也可以删除；
4. 创建新用户不验证用户是否填写了邮箱；

## INSTALLATION

Required step:

Enable the module as you normally would. See:
https://www.drupal.org/docs/8/extending-drupal-8/installing-modules

## CONFIGURATION

You can enter the account setting interface to manage the user mailbox field.
Setting path: "admin/config/people/accounts"
