<?php

namespace Drupal\user_mail_control\Plugin\Validation\Constraint;

use Drupal\user\Plugin\Validation\Constraint\UserMailRequired;

/**
 * Disable email address is required.
 *
 * @Constraint(
 *   id = "UserMailRequiredDisabled",
 *   label = @Translation("User Mail Required Disabled", context = "Validation")
 * )
 */
class UserMailRequiredDisabled extends UserMailRequired {

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return UserMailRequiredDisabledValidator::class;
  }

}
