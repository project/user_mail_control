<?php

namespace Drupal\user_mail_control\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Disable email address is required validation.
 *
 * @package Drupal\user_mail_control\Plugin\Validation\Constraint
 */
class UserMailRequiredDisabledValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $items */
    $values = $items->getValue();
    if (empty($values['value'])) {
      return NULL;
    }
  }

}
